#include <Adafruit_NeoPixel.h> //library for controlling the RGB-LED strip

#define PIN 9 //data input via PIN 9 on Arduino

// Parameter 1 = number of pixels in strip
// Parameter 2 = pin number (most are valid)
// Parameter 3 = pixel type flags, add together as needed:
//   NEO_KHZ800  800 KHz bitstream (most NeoPixel products w/WS2812 LEDs)
//   NEO_KHZ400  400 KHz (classic 'v1' (not v2) FLORA pixels, WS2811 drivers)
//   NEO_GRB     Pixels are wired for GRB bitstream (most NeoPixel products)
//   NEO_RGB     Pixels are wired for RGB bitstream (v1 FLORA pixels, not v2)
Adafruit_NeoPixel strip = Adafruit_NeoPixel(60, PIN, NEO_GRB + NEO_KHZ800);


//3e etage
//bool VergaderruimteTongelreep;        1 [0]
//bool VergaderruimteKruisstraat;       2 [1]
//bool VergaderruimteWitteDame;         3 [2]
//bool Limbopad;                        4 [3]
//bool RustruimteStroomhuisje;          5 [4]
//bool VergaderruimteKleineBerg;        6 [5]
//bool VergaderruimteVanAbbe;           7 [6]

//4e etage
//bool VergaderruimteStrijpS;           8 [7]
//bool VergaderruimteStrijpR;           9 [8]
//bool VergaderruimteParktheater;       10 [9]
//bool VergaderruimteDeBerenkuil;       11 [10]
//bool VergaderruimteHighTechCampus;    12 [11]
//bool VergaderruimteStratum;           13 [12]
//bool VergaderruimteLasVegas;          14 [13]
//bool VergaderruimteAbbeyRoad;         15 [14]

//1e etage Labs
//bool VergaderruimteKnooppunt;         16 [15]
//bool VergaderruimteVanMoll;           17 [16]
//bool VergaderruimteDeBurger;          18 [17]

//2e etage Kantine
//bool VergaderruimteBrainstormruimte;  19 [18]
//nobool FlatIronBuilding;              x
//bool VergaderruimteAsianMarket;       20 [19]
//bool KantineGreenhouseGroup;          21 [20]

//3e etage Blossom
//bool VergaderruimteWoenselWest;       22 [21]
//bool VergaderruimteDeHoogsteTijd;     23 [22]
//bool VergaderruimteHetKetelhuis;      24 [23]


int internList[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23};
int externList[] = {0, 0, 0, 2, 2, 0, 2, 2, 0, 0, 2, 0, 2, 0, 0, 2, 2, 0, 0, 2, 2, 0, 2, 0};
const int leds = 23; //amount of leds = 24. counting starts at 0 (included the 0)



String iets = "";
//0 is False and every other number is True
void setup() {
  // setup code, to run once:
  Serial.begin(9600);
  strip.begin();
}

void loop() {

  // send data only when you receive data:
  if (Serial.available() > 0) {
    String receivedData = Serial.readString(); //trough the serialport connection, a string with numbers will be read
    Serial.println("true\r\n");
    Serial.flush(); //the string of numbers will be converted to an array
      // read the incoming byte:
      
//      delay(20000);
  
      // say what you got:
      
      for (int i = 0; i < receivedData.length(); i++) // LED 1
    {
      char x = receivedData[i]; //in a char loop, the array will be read and the number of the meeting room will be connected to the number of the led-strip, which is already sorted on this array
        if (x == 48) //48 is ascii voor 0, 49 == 1, 50 == 2
        {
          strip.setPixelColor(i, 0, 255, 0);  // set LED 1 to the color in (green) [intensity is lower number under 255]
        }
        else if(x == 49)
        {
          strip.setPixelColor(i, 255, 0, 0);  // set LED 1 to the color in (red) [intensity is lower number under 255]
        }
        else 
        {
          strip.setPixelColor(i, 255, 0, 0);  // set LED 1 to the color in (red) [intensity is lower number under 255]
        }
    }
    strip.show();//in CMD will be shown what the result is
    receivedData = ""; 
    
  } 
}
