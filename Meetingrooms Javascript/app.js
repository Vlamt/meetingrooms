console.log("script start");

var moment = require('moment');
var EWS = require('node-ews')

var config = require('./config.json');



//uncomment vanuit hier

function start(){
    // Exchange server connection info
    var ewsConfig = {
        username: config.username,
        password: config.password,
        host: 'https://webmail.bluemango.nl'
    };

    process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0"; 

    //set times
    var tijd = moment().format('YYYY-MM-DDTHH:mm:ss');
    var dadelijk = moment(tijd).add(5, 'minutes');
    dadelijk = moment(dadelijk).format('YYYY-MM-DDTHH:mm:s');

    var ewsSoapHeader = {
      't:RequestServerVersion': { 
        attributes: {
          Version: "Exchange2010"
        }
      }
    };

    // initialize node-ews
    var ews = new EWS(ewsConfig);

    var ewsFunction = 'GetRoomLists';

    var ewsAvailFunction = 'GetUserAvailability';
    var ewsAvail = {
        'TimeZone':{
            'Bias' : '-60',
            'StandardTime':{
                'Bias':'0',
                'Time':'02:00:00',
                'DayOrder':'5',
                'Month':'10',
                'DayOfWeek':'Sunday'
            },
            'DaylightTime':{
                'Bias':'0',
                'Time':'02:00:00',
                'DayOrder':'1',
                'Month':'4',
                'DayOfWeek':'Sunday'
            }
        },
        'MailboxDataArray':{ //deze array aanroepen
            'MailboxData':[{ //of juist deze
                'Email':{
                    //'Name':'',
                    'Address': 'tongelreep@bluemango.nl',
                    //'RoutingType':''
                },
                    'AttendeeType':'Organizer',
                    'ExcludeConflicts':'false'
                }
            ,
            {
                'Email':{
                    //'Name':'',
                    'Address': 'kruisstraat@bluemango.nl',
                    //'RoutingType':''
                },
                    'AttendeeType':'Organizer',
                    'ExcludeConflicts':'false'
                }
            ,
            {
                'Email':{
                    //'Name':'',
                    'Address': 'witte.dame@bluemango.nl',
                    //'RoutingType':''
                },
                    'AttendeeType':'Organizer',
                    'ExcludeConflicts':'false'
                }
            ,
            {
                'Email':{
                    //'Name':'',
                    'Address': 'limbopad@bluemango.nl',
                    //'RoutingType':''
                },
                    'AttendeeType':'Organizer',
                    'ExcludeConflicts':'false'
                }
            ,
            {
                'Email':{
                    //'Name':'',
                    'Address': 'rustruimte.stroomhuisje@bluemango.nl',
                    //'RoutingType':''
                },
                    'AttendeeType':'Organizer',
                    'ExcludeConflicts':'false'
                }
            ,
            {
                'Email':{
                    //'Name':'',
                    'Address': 'kleineberg@bluemango.nl',
                    //'RoutingType':''
                },
                    'AttendeeType':'Organizer',
                    'ExcludeConflicts':'false'
                }
            ,
            {
                'Email':{
                    //'Name':'',
                    'Address': 'vanabbe@bluemango.nl',
                    //'RoutingType':''
                },
                    'AttendeeType':'Organizer',
                    'ExcludeConflicts':'false'
                }
            ,
            {
                'Email':{
                    //'Name':'',
                    'Address': 'strijps@bluemango.nl',
                    //'RoutingType':''
                },
                    'AttendeeType':'Organizer',
                    'ExcludeConflicts':'false'
                }
            ,
            {
                'Email':{
                    //'Name':'',
                    'Address': 'strijpr@bluemango.nl',
                    //'RoutingType':''
                },
                    'AttendeeType':'Organizer',
                    'ExcludeConflicts':'false'
                }
            ,
            {
                'Email':{
                    //'Name':'',
                    'Address': 'parktheater@bluemango.nl',
                    //'RoutingType':''
                },
                    'AttendeeType':'Organizer',
                    'ExcludeConflicts':'false'
                }
            ,
            {
                'Email':{
                    //'Name':'',
                    'Address': 'deberenkuil@bluemango.nl',
                    //'RoutingType':''
                },
                    'AttendeeType':'Organizer',
                    'ExcludeConflicts':'false'
                }
            ,
            {
                'Email':{
                    //'Name':'',
                    'Address': 'hightechcampus@bluemango.nl',
                    //'RoutingType':''
                },
                    'AttendeeType':'Organizer',
                    'ExcludeConflicts':'false'
                }
            ,
            {
                'Email':{
                    //'Name':'',
                    'Address': 'stratum@bluemango.nl',
                    //'RoutingType':''
                },
                    'AttendeeType':'Organizer',
                    'ExcludeConflicts':'false'
                }
            ,
            {
                'Email':{
                    //'Name':'',
                    'Address': 'lasvegas@bluemango.nl',
                    //'RoutingType':''
                },
                    'AttendeeType':'Organizer',
                    'ExcludeConflicts':'false'
                }
            ,
            {
                'Email':{
                    //'Name':'',
                    'Address': 'abbeyroad@bluemango.nl',
                    //'RoutingType':''
                },
                    'AttendeeType':'Organizer',
                    'ExcludeConflicts':'false'
                }
            ,
            {
                'Email':{
                    //'Name':'',
                    'Address': 'knooppunt@bluemango.nl',
                    //'RoutingType':''
                },
                    'AttendeeType':'Organizer',
                    'ExcludeConflicts':'false'
                }
            ,
            {
                'Email':{
                    //'Name':'',
                    'Address': 'vanmoll@bluemango.nl',
                    //'RoutingType':''
                },
                    'AttendeeType':'Organizer',
                    'ExcludeConflicts':'false'
                }
            ,
            {
                'Email':{
                    //'Name':'',
                    'Address': 'deburger@bluemango.nl',
                    //'RoutingType':''
                },
                    'AttendeeType':'Organizer',
                    'ExcludeConflicts':'false'
                }
            ,
            {
                'Email':{
                    //'Name':'',
                    'Address': 'brainstormruimte@bluemango.nl',
                    //'RoutingType':''
                },
                    'AttendeeType':'Organizer',
                    'ExcludeConflicts':'false'
                }
            ,
            {
                'Email':{
                    //'Name':'',
                    'Address': 'asianmarket@bluemango.nl',
                    //'RoutingType':''
                },
                    'AttendeeType':'Organizer',
                    'ExcludeConflicts':'false'
                }
            ,
            {
                'Email':{
                    //'Name':'',
                    'Address': 'kantine@greenhousegroup.com',
                    //'RoutingType':''
                },
                    'AttendeeType':'Organizer',
                    'ExcludeConflicts':'false'
                }
            ,
            {
                'Email':{
                    //'Name':'',
                    'Address': 'woenselwest@bluemango.nl',
                    //'RoutingType':''
                },
                    'AttendeeType':'Organizer',
                    'ExcludeConflicts':'false'
                }
            ,
            {
                'Email':{
                    //'Name':'',
                    'Address': 'dehoogstetijd@bluemango.nl',
                    //'RoutingType':''
                },
                    'AttendeeType':'Organizer',
                    'ExcludeConflicts':'false'
                }
            ,
            {
                'Email':{
                    //'Name':'',
                    'Address': 'hetketelhuis@bluemango.nl',
                    //'RoutingType':''
                },
                    'AttendeeType':'Organizer',
                    'ExcludeConflicts':'false'
                }
            ]},
            'FreeBusyViewOptions':{
            'TimeWindow':{  
                'StartTime':tijd,
                'EndTime':dadelijk
        },
            'MergedFreeBusyIntervalInMinutes':'5',
            'RequestedView':'MergedOnly'
        }
    };
    
    //get data functie
    ews.run(ewsAvailFunction, ewsAvail)
      .then(result => {
        verwerkData(result);
      })
      .catch(err => {
        return (err.stack);
      });
}

function verwerkData(json) {
    console.log("verwerk Data");

    var myThings = json.FreeBusyResponseArray.FreeBusyResponse;
    var myData = '';
    for(k in myThings) {
        myData = myData + (myThings[k].FreeBusyView.MergedFreeBusy);
    };
    
    arduinoVerstuur(myData);
}

function arduinoVerstuur(argument) {
    console.log(argument);
    port.write(argument, function(err) {
      if (err) {
        console.log('Error on write: ', err.message);
      }
    console.log('message written');
    port.drain();
    })

    
    // Open errors will be emitted as an error event
    port.on('error', function(err) {
      console.log('Error: ', err.message);
    })
}



//tot hier.
const SerialPort = require('serialport');
const Readline = require('@serialport/parser-readline');
const port = new SerialPort('COM3', {
    baudRate: 9600
});
//opent de arduino
const parser = port.pipe(new Readline({ delimiter: '\r\n' }))
parser.on('data', console.log)

port.on("open", function () {
    console.log('open');
});


setTimeout(function(){
    start();
    //interval voor het updaten van freebusy per 5 minuten
    setInterval(function(){ 
        start();
    }, 300000); //5 minuten
}, 5000);


//0 Free 
//1 Tentative 
//2 Busy 
//3 Out of Office (OOF) 
//4 No data 


